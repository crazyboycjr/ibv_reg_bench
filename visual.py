#!/usr/bin/env python2

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt


def to_list(df, col):
    return list(df[col].values)


def convert(x):
    if x < 1024:
        return str(x) + 'B'
    if x < 1048576:
        return str(x / 1024) + 'KB'
    if x < 1024**3:
        return str(x / 1024 / 1024) + 'MB'
    return str(x / 1024**3) + 'GB'


def main():
    df = pd.read_csv('bench_result.csv', sep=',')
    x_axis = to_list(df, 'Size(bytes)')
    x_axis = map(convert, x_axis)
    print(x_axis)
    print(to_list(df, 'Size(bytes)'))
    print(to_list(df, 'Register(ns)'))
    print(to_list(df, 'Memcopy hot(ns)'))
    print(to_list(df, 'Memcopy cold(ns)'))


if __name__ == '__main__':
    main()
