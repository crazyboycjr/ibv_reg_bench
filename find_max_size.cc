#include <infiniband/verbs.h>
#include <unistd.h>
#include <fstream>
#include <iostream>

#include "verbs_ctx.h"

VerbsContext context;

template <typename T> static inline T DivUp(T x, T y) { return (x + y - 1) / y; }
template <typename T> static inline T RoundUp(T x, T y) { return DivUp(x, y) * y; }

void PageAlignedMalloc(void** ptr, size_t size) {
  size_t page_size = sysconf(_SC_PAGESIZE);
  void* p;
  size_t size_aligned = RoundUp(size, page_size);
  int ret = posix_memalign(&p, page_size, size_aligned);
  if (ret) {
    fprintf(stderr, "posix_memalign error: %s\n", strerror(ret));
    abort();
  }
  assert(p);
  // memset(p, 0, size);
  *ptr = p;
}

size_t GetMemAvailable() {
  std::string token;
  std::ifstream file("/proc/meminfo");
  while (file >> token) {
    if (token == "MemAvailable:") {
      size_t mem;
      if (file >> mem) {
        return mem;
      } else {
        return 0;
      }
    }
    // ignore rest of the line
    file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  }
  return 0;  // nothing found
}

bool CheckAllocRegister(size_t size) {
  std::cout << "Checking for size: " << size << std::endl;
  bool ret = true;
  void* ptr = nullptr;
  PageAlignedMalloc(&ptr, size);
  if (ptr == nullptr) {
    std::cout << "size: " << size << " allocate failed, errno: " << errno << std::endl;
    return false;
  }
  struct ibv_mr* mr = ibv_reg_mr(context.pd(), ptr, size, IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_READ);
  if (mr == nullptr) {
    ret = false;
    std::cout << "size: " << size << " register failed, errno: " << errno << std::endl;
  }
  if (mr) ibv_dereg_mr(mr);
  if (ptr) free(ptr);
  return ret;
}

size_t FindMaxSize() {
  size_t mem_avail = GetMemAvailable();
  std::cout << "MemAvailable: " << mem_avail << " kB" << std::endl;
  size_t r = mem_avail * 1024;
  size_t l = 0;
  while (l < r - 1) {
    auto m = (l + r) / 2;
    if (CheckAllocRegister(m)) l = m;
    else r = m;
  }
  if (CheckAllocRegister(r)) return r;
  return l;
}

size_t FindMaxTimes(size_t max_size) {
  size_t ans = 0;
  char* ptr = nullptr;
  size_t page_size = 1024 * sysconf(_SC_PAGESIZE);
  PageAlignedMalloc((void**)&ptr, max_size);
  while (1) {
    struct ibv_mr* mr = ibv_reg_mr(context.pd(), ptr + ans * page_size, page_size, IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_READ);
    ans++;
    std::cout << "register times: " << ans << ", accumulated size: " << ans * page_size << std::endl;
    if (mr == nullptr) {
      std::cout << "times: " << ans << " register failed, errno: " << errno << std::endl;
      break;
    }
  }
  return ans;
}

int main() {
  int err = context.Init("mlx5_0");
  assert(!err);

  // size_t max_reg_size = FindMaxSize();
  // std::cout << "Found max size can be registered: " << max_reg_size / 1024 << " kB" << std::endl;

  size_t max_reg_times = FindMaxTimes(1024ll * 1024 * 1024ll * 64);
  std::cout << "Found max times can be registered for page size: " << max_reg_times << std::endl;
  return 0;
}
