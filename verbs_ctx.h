#include <infiniband/verbs.h>
#include <cassert>
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <functional>
#include <string>
#include <vector>
#include <cstring>

class VerbsContext {
 public:
  VerbsContext() {}
  int Init(const std::string& dev_name) {
    int failed = 0;
    int dev_cnt;
    dev_list_ = ibv_get_device_list(&dev_cnt);
    printf("found %d devices\n", dev_cnt);
    if (dev_cnt == 0) {
      failed = 1;
      Release();
      return 1;
    }

    struct ibv_device* dev = nullptr;
    for (int i = 0; i < dev_cnt; i++) {
      const char* name = ibv_get_device_name(dev_list_[i]);
      if (std::string(name) == dev_name) {
        dev = dev_list_[i];
        break;
      }
    }

    if (!dev) {
      printf("no matching device found\n");
      failed = 1;
      Release();
      return 1;
    }

    verbs_ = ibv_open_device(dev);
    if (!verbs_) {
      printf("ibv_open_device failed");
      Release();
      return 1;
    }

    pd_ = ibv_alloc_pd(verbs_);
    if (!pd_) {
      printf("ibv_alloc_pd failed");
      Release();
      return 1;
    }

    return failed;
  }

  void Release() {
    if (pd_) {
      ibv_dealloc_pd(pd_);
      pd_ = nullptr;
    }
    if (verbs_) {
      ibv_close_device(verbs_);
      verbs_ = nullptr;
    }
    if (dev_list_) {
      ibv_free_device_list(dev_list_);
      dev_list_ = nullptr;
    }
  }

  struct ibv_pd* pd() const {
    return pd_;
  }

 private:
  struct ibv_pd* pd_{nullptr};
  struct ibv_context* verbs_{nullptr};
  struct ibv_device** dev_list_{nullptr};
};
