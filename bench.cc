#include "verbs_ctx.h"

const size_t kMaxSize = 1 << 28;
const int access_flags = IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_READ;

using namespace std::chrono;
using namespace std::chrono_literals;
using Clock = std::chrono::steady_clock;
using Func = std::function<void()>;

template <typename Period>
Period Measure(const Func& func, int repeat) {
  auto start = Clock::now();
  for (int i = 0; i < repeat; i++) func();
  auto end = Clock::now();

  return duration_cast<Period>(end - start) / repeat;
}

template <typename Period>
Period Benchmark(const Func& func) {
  auto dura = Measure<nanoseconds>(func, 100);
  auto sec_in_nano = duration_cast<nanoseconds>(1s);
  int warm_up_repeat;
  if (dura.count() > 0)
    warm_up_repeat = sec_in_nano / dura / 100 + 10;
  else
    warm_up_repeat = 1000;

  // warm up
  for (int i = 0; i < warm_up_repeat; i++) func();

  dura = Measure<nanoseconds>(func, warm_up_repeat * 100);
  return duration_cast<Period>(dura);
}

VerbsContext context;

void Register(void* addr, size_t size) {
  struct ibv_mr* mr = ibv_reg_mr(context.pd(), addr, size, access_flags);
  assert(mr);
  ibv_dereg_mr(mr);
}

#define ROUNDDOWN(x, d) ((x) - (x) % (d))

int main() {
  srand(unsigned(time(0)));
  // auto func = [] { int a = 10; };
  // Benchmark(func);
  int err = context.Init("mlx5_0");
  assert(!err);

  std::vector<size_t> size_vec;
  for (int i = 0; (1u << i) <= kMaxSize; i++) size_vec.push_back(1 << i);

  char* buffer = static_cast<char*>(malloc(kMaxSize));

  printf("Size(bytes)\tRegister(ns)\tMemcopy hot(ns)\tMemcopy cold(ns)\n");
  for (size_t i = 0; i < size_vec.size(); i++) {
    size_t size = size_vec[i];
    // register
    auto dura1 = Benchmark<nanoseconds>([buffer, size] { Register(buffer, size); });

    // mem copy hot
    char* new_buffer = static_cast<char*>(malloc(size));
    int offset = (ssize_t)rand() % kMaxSize - size;
    if (offset < 0) offset = 0;
    offset = ROUNDDOWN(offset, 256);
    auto dura2 = Benchmark<nanoseconds>(
        [buffer, new_buffer, offset, size] { memcpy(buffer + offset, new_buffer, size); });

    // mem copy cold
    auto dura3 = Benchmark<nanoseconds>([buffer, new_buffer, size] {
      int offset = (ssize_t)rand() % kMaxSize - size;
      if (offset < 0) offset = 0;
      offset = ROUNDDOWN(offset, 256);
      memcpy(buffer + offset, new_buffer, size);
    });

    free(new_buffer);

    printf("% 10ld\t% 12ld\t% 15ld\t% 16ld\n", size, dura1.count(), dura2.count(), dura3.count());
  }

  free(buffer);
  return 0;
}
