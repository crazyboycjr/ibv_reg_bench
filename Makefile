.PHONY: clean all

CFLAGS := -O3 -Wall -std=c++14 -g
LDFLAGS := -libverbs

APP := bench find_max_size

all: $(APP)

bench: bench.cc
	g++ $^ -o $@ $(CFLAGS) $(LDFLAGS)

find_max_size: find_max_size.cc
	g++ $^ -o $@ $(CFLAGS) $(LDFLAGS)

clean:
	$(RM) $(APP)
