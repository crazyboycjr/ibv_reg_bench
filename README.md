# ibv\_reg\_mr VS memcpy

This repo measures the overhead of `ibv_reg_mr + ibv_dereg_mr` and `memcpy`.

## Get started
```
% make
% ./bench
found 2 devices
Size(bytes)	Register(ns)	Memcopy hot(ns)	Memcopy cold(ns)
         1	       25384	              5	             344
         2	       25687	              5	             102
         4	       25710	              5	             106
         8	       25963	              5	              96
        16	       25681	              5	              95
        32	       24981	              4	              95
        64	       24945	              5	             104
       128	       27286	              7	             228
       256	       25195	             11	             404
       512	       25793	             15	             408
      1024	       25499	             28	             492
      2048	       24892	             58	             568
      4096	       25864	            112	             808
      8192	       25822	            218	            1310
     16384	       26067	            522	            2342
     32768	       26184	           1620	            4408
     65536	       27241	           3058	            8455
    131072	       28896	           5372	           16498
    262144	       32368	          12335	           32084
    524288	       39406	          30825	           64670
   1048576	       54619	          58536	          129130
   2097152	       85407	         116355	          257029
   4194304	      146078	         234939	          517071
   8388608	      266688	         470284	         1030504
  16777216	      503410	         963764	         2097385
  33554432	      968804	        3347507	         4242656
  67108864	     1903203	        8645790	         8523723
 134217728	     3773284	       17505163	        17566431
 268435456	     7646815	       35346592	        35206724
./bench  196.37s user 36.80s system 97% cpu 3:59.76 total
```

Copy the results into excel and export to `bench_result.csv`, then run
```
python2 visual.py
```
This script will output data needed in `index.html`, subsititude the data in `index.html` with your benchmark results.

Finally, open `index.html` and see the chart.
![screenshot](https://bitbucket.org/crazyboycjr/ibv_reg_bench/raw/master/screenshot.png)

## LICENSE
MIT License

Copyright (c) 2018 Jingrong Chen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
